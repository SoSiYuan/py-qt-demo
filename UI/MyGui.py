import configparser
import random

# from PyQt5.Qt import *  # 导入常用的Qt插件
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QWidget, QMessageBox
from plugin.Log.LogInFile import LogInFile

from UI.PyQtDemo import Ui_MainWindow
from plugin.CfgManager.CfgSave import CfgFileManager


class ButtonText:
    def __init__(self, iStartText="start", iStopText="stop"):
        self.startText = iStartText
        self.stopText = iStopText


class MyWindows(Ui_MainWindow):
    def __init__(self):
        super(MyWindows, self).__init__()
        # 共有属性
        self.qTimer = None  # 定时器
        self.cfm = CfgFileManager()  # 配置管理器
        # 私有属性
        self.__buttonText = ButtonText("开始", "暂停")  # 按钮文字配置
        self.__logInFile = LogInFile("Roller.log")  # 记录日志
        self.__refreshCycle = 50  # 数码管刷新周期
        self.__stuNum = 50
        self.__startFlag = 0

        # 从配置文件读取配置信息
        self.GetAllCfgFromIni()

    def setupUi(self, MainWindow):
        super(MyWindows, self).setupUi(MainWindow)

        # UI调整
        self.qTimer = QTimer(MainWindow);
        self.LE_RefreshStep.setText(str(self.__refreshCycle))
        self.LE_StuNum.setText(str(self.__stuNum))

        # 信号链接
        self.qTimer.timeout.connect(lambda: self.ShowNumTimer())
        self.LE_StuNum.editingFinished.connect(lambda: self.SetStuNum(self.LE_StuNum.text()))
        self.LE_RefreshStep.editingFinished.connect(lambda: self.SetRefreshStep(self.LE_RefreshStep.text()))
        self.PB_Roller.clicked.connect(lambda: self.PB_Click())
        self.RB_LogSwitch.clicked.connect(lambda: self.__logInFile.LogSwitch())  # 抽号日志开关

    def GetAllCfgFromIni(self):
        # 从配置文件中读取值
        self.__refreshCycle = self.GetCfgFromIni("self.__refreshCycle")
        self.__stuNum = self.GetCfgFromIni("self.__stuNum")

    def GetCfgFromIni(self, Section):
        val = 50
        try:
            val = self.cfm.Get(Section)
            if val is None:
                val = 50
            else:
                val = eval(val)
        except configparser.NoSectionError:
            pass
        return val

    def SaveCfgToIni(self, Section, val):
        try:
            self.cfm.Save(str(Section), str(val))
            self.__logInFile.Log(str(Section) + ' = ' + str(val))
        except:
            pass

    def SetStuNum(self, num):
        try:
            self.__stuNum = int(eval(str(num)))
            self.SaveCfgToIni("self.__stuNum", self.__stuNum)
            self.LE_StuNum.setText(str(self.__stuNum))
        except:
            self.LE_StuNum.setText(str(self.__stuNum))
            msg_box = QMessageBox(QMessageBox.Critical, '错误', '设置为:' + str(num) + '[失败]，请检查')
            msg_box.exec_()
            pass

    def SetRefreshStep(self, num):
        try:
            self.__refreshCycle = float(eval(str(num)))
            self.SaveCfgToIni("self.__refreshCycle", self.__refreshCycle)
        except:
            self.LE_RefreshStep.setText(str(self.__refreshCycle))
            msg_box = QMessageBox(QMessageBox.Critical, '错误', '设置为:' + str(num) + '[失败]，请检查')
            msg_box.exec_()

    # 改变开关状态
    def SwitchFlag(self):
        self.__startFlag = (self.__startFlag + 1) % 2

    def PB_Click(self):
        self.SwitchFlag()
        if self.__startFlag == 1:
            # 启动
            self.__logInFile.Log(self.__buttonText.startText)
            random.seed()
            self.qTimer.start(self.__refreshCycle)
            self.PB_Roller.setText(self.__buttonText.stopText)
        else:
            # 暂停
            self.__logInFile.Log(self.__buttonText.stopText)
            self.qTimer.stop()
            self.PB_Roller.setText(self.__buttonText.startText)

    def ShowNumTimer(self):
        num = random.randint(1, self.__stuNum)
        self.lcdNumber.display(num)
        self.__logInFile.Log(num)


class MyGui(QWidget):

    def __init__(self):
        super(MyGui, self).__init__()

    def setCentralWidget(self, tmp):
        pass

    def setStatusBar(self, tmp):
        pass

    def setMenuBar(self, tmp):
        pass
