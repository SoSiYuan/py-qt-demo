import configparser


class CfgFileManager:
    def __init__(self):
        self.__fileName = "Roller.ini"
        self.cp = configparser.ConfigParser()
        self.cp.read(self.__fileName)
        try:
            self.cp.add_section("db")
        except configparser.DuplicateSectionError:
            pass

    def Save(self, option, val):
        self.cp.set("db", str(option), str(val))
        fp = open(self.__fileName, 'w', encoding="utf-8")
        self.cp.write(fp)
        fp.close()

    def Get(self, section):
        val = None
        try:
            val = self.cp.get("db", section)
        except configparser.NoOptionError:
            pass
        return val

    def Show(self):
        print('host of db:', self.cp.items("db"))

    def Delete(self, option):
        self.cp.remove_option(option)
