class LogInFile:
    def __init__(self, fileName='log.log'):
        self.__fileName = fileName  # 日志文件名
        self.__logSwitch = False  # 日志开关

    def LogSwitch(self):
        self.__logSwitch = not self.__logSwitch

    def Log(self, data):
        if self.__logSwitch:
            print("debug:" + str(data))
            return
        pf = open(self.__fileName, "a+")
        pf.write(str(data))
        pf.write("\n")
        pf.close()
