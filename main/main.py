import sys
from PyQt5.QtWidgets import QApplication
from UI.MyGui import MyWindows, MyGui
if __name__ == '__main__':
    app = QApplication(sys.argv)  # 创建应用程序对象, sys.argv同c++的argv。全局对象:qApp
    # ===控件操作开始===
    # 1.1 创建控件
    widget = MyGui()
    mainWindow = MyWindows()
    mainWindow.setupUi(widget)  # 将ui文件生成的框架注册到widget中

    widget.show()
    # ===应用程序的执行===
    sys.exit(app.exec_())  # 程序的生命周期跟随app窗口, 并启动监听循环
